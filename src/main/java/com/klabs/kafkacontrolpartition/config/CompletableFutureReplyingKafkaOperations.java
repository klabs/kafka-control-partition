package com.klabs.kafkacontrolpartition.config;

import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;

public interface CompletableFutureReplyingKafkaOperations<K, V, R> {
    CompletableFuture<R> requestReply(ProducerRecord<K, V> record);

    CompletableFuture<R> requestReply(ProducerRecord<K, V> record, Duration replyTimeout);
}
